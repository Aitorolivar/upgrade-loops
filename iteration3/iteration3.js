// Usa un bucle forof para recorrer todos los destinos del array. Imprime en un ***console.log*** sus valores.

// Puedes usar este array:



const placesToTravel = ['Japon', 'Venecia', 'Murcia', 'Santander', 'Filipinas', 'Madagascar'];

for(let place of placesToTravel){
    console.log(`I will be traveling to: ${place}`);
};
